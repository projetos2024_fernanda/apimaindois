const router = require('express').Router()
const alunoController = require('../controller/alunoController')
const teacherController = require('../controller/teacherController')
const JSONPlaceholderController = require('../controller/JSONPlaceholderController')
const dbController = require('../controller/dbController')
const clienteController = require('../controller/clientecontroller')
const pizzariaController = require('../controller/pizzariaController')


router.get('/docente/', teacherController.getTeachers)
router.post('/cadastroaluno/', alunoController.postAluno)
router.put('/updatealuno/', alunoController.updateAluno)
router.delete('/deletealuno/:id', alunoController.deleteAluno)

router.get('/external/', JSONPlaceholderController.getUsers)
router.get('/external/io', JSONPlaceholderController.getUsersWebsiteIO)
router.get('/external/com', JSONPlaceholderController.getUsersWebsiteCOM)
router.get('/external/net', JSONPlaceholderController.getUsersWebsiteNET)

router.get('/external/filter', JSONPlaceholderController.getCountDomain)

router.get('/tables', dbController.getNameTables)
router.get('/tablesd', dbController.getTablesDescription)

router.post('/cadastrocliente/', clienteController.createCliente)
router.get('/buscarcliente/', clienteController.getAllClientes)
router.get('/procurarcliente/', clienteController.getAllClientes2)


router.get('/procurarpizza/', pizzariaController.listarPedidosPizza)

router.get('/procurarpizzas/', pizzariaController.listarPedidosPizza2)

router.get('/procurarpizzasjoin/', pizzariaController.listarPedidosPizzasComJoin)



module.exports = router;