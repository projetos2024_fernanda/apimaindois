const connect = require("../db/connect");
//Obtendo conexão com o banco de dados

module.exports = class pizzariaController {
  static async listarPedidosPizza(req, res) {
    const query = `SELECT pp.fk_id_pedido as Pedido, p.nome as Pizza, pp.quantidade as Qtde,
        round((pp.valor/pp.quantidade),2) as Unitário, pp.valor as Total from pizza_pedido pp,
        pizza p where pp.fk_id_pizza = p.id_pizza order by pp.fk_id_pedido;`;
    //fim da atribuição da constante

    try {
      connect.query(query, function (err, result, fields) {
        if (err) {
          console.log(err);
          res.status(500).json({
            error: "Erro ao consultar pedidos de pizzas no banco de dados ",
          });
          return;
        }
        console.log("Consulta realizada com sucesso!");
        res.status(201).json({ result });
        //result é a resposta (positiva) da requisição
      }); // fim da connect query
      //err(try) mostra o erro de compilação
    } catch (error) {
      console.log("Erro ao executar a consulta de pedidos de pizzas", error);
      res.status(500).json({ error: "Erro interno do servidor" });
      //error(catch) mostra um erro de sintaxe
    } // fim do catch
  } // fim do listarPedidosPizza



  static async listarPedidosPizza2(req, res) {
    const query = `SELECT * from pizza_pedido, pizza;`;
    //fim da atribuição da constante

    try {
      connect.query(query, function (err, result, fields) {
        if (err) {
          console.log(err);
          res.status(500).json({
            error: "Erro ao consultar pedidos de pizzas no banco de dados ",
          });
          return;
        }
        console.log("Consulta realizada com sucesso!");
        res.status(201).json({ result });
        //result é a resposta (positiva) da requisição
      }); // fim da connect query
      //err(try) mostra o erro de compilação
    } catch (error) {
      console.log("Erro ao executar a consulta de pedidos de pizzas", error);
      res.status(500).json({ error: "Erro interno do servidor" });
      //error(catch) mostra um erro de sintaxe
    } // fim do catch
  } // fim do listarPedidosPizza2


  static async listarPedidosPizzasComJoin(req, res) {
    const query = `SELECT pp.fk_id_pedido as Pedido, p.nome as Pizza, pp.quantidade as Qtde, round((pp.valor/pp.quantidade),2) as Unitário, pp.valor as Total
    from pizza_pedido pp INNER JOIN pizza p on pp.fk_id_pizza = p.id_pizza order by pp.fk_id_pedido;;`;
    //fim da atribuição da constante

    try {
      connect.query(query, function (err, result, fields) {
        if (err) {
          console.log(err);
          res.status(500).json({
            error: "Erro ao consultar pedidos de pizzas no banco de dados ",
          });
          return;
        }
        console.log("Consulta realizada com sucesso!");
        res.status(201).json({ result });
        //result é a resposta (positiva) da requisição
      }); // fim da connect query
      //err(try) mostra o erro de compilação
    } catch (error) {
      console.log("Erro ao executar a consulta de pedidos de pizzas", error);
      res.status(500).json({ error: "Erro interno do servidor" });
      //error(catch) mostra um erro de sintaxe
    } // fim do catch
  } // fim do listarPedidosPizzasComJoin


  
}; // fim do module exports
