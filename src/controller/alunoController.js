module.exports = class alunoController {
  static async postAluno(req, res) {
    console.log("Aluno", req.body);
  }

  static async updateAluno(req, res) {
    const { nome, idade, profissao, cursoMatriculado } = req.body;
    // Validando se há algum campo em branco
    if (
      nome !== "" &&
      idade !== 0 &&
      profissao !== "" &&
      cursoMatriculado != ""
    ) {
      return res.status(200).json({ message: "Aluno editado" });
    } else {
      return res
        .status(400)
        .json({ message: "Favor preencher todos os campos" });
    }
  }

  static async deleteAluno(req, res) {
    if (req.params.id !== "") {
      return res.status(200).json({ message: "Aluno removido" });
    }
  }
};
