const connect = require("../db/connect");
//Obtendo conexão com o banco de dados

module.exports = class clienteController {
  static async createCliente(req, res) {
    const {
      telefone,
      nome,
      cpf,
      logradouro,
      numero,
      complemento,
      bairro,
      cidade,
      estado,
      cep,
      referencia,
    } = req.body;

    //verificando se o atributo chave é diferente de zero
    if (telefone !== 0) {
      const query = `insert into cliente (telefone, nome, cpf, logradouro, numero, complemento, bairro, cidade, estado, cep, referencia) values (
                '${telefone}',
                '${nome}',
                '${cpf}',
                '${logradouro}',
                '${numero}',
                '${complemento}',
                '${bairro}',
                '${cidade}',
                '${estado}',
                '${cep}',
                '${referencia}'
                
            )`; //fim da query

      try {
        connect.query(query, function (err) {
          if (err) {
            console.log(err);
            res.status(500).json({ error: "Não cadastrado no banco" });
            return;
          }
          console.log("Inserido no banco!");
          res.status(201).json({ message: "Criado com sucesso" });
        });
      } catch (error) {
        console.log("Erro ao executar o insert!", error);
        res.status(500).json({ error: "Erro interno do servidor" });
      }
    } //fim do if
    else {
      res.status(400).json({ message: "O telefone é obrigatório!!" });
    } //fim do else
    //pegar as informações que vêm da requisição
  } //fim do createCliente
  static async getAllClientes(req, res) {
    const query = `SELECT * from cliente`;
    try {
      connect.query(query, function (err, data) {
        if (err) {
          console.log(err);
          res.status(500).json({ error: "Usuários não encontrados no banco" });
          return;
        } //fim do if
        let clientes = data;
        console.log("Consulta realizada com sucesso!");
        res.status(201).json({ clientes });
        return;
      }); //fim do connect
    } catch (error) {
      console.error("Erro ao executar a consulta: ", error);
      res.status(500).json({ error: "Erro interno no servidor" });
      return;
    }
  } // fim do getAllClientes

  static async getAllClientes2(req, res) {
    //método para selecionar clientes via parâmetros específicos

    //extrair parâmetros da consulta da URL
    const { filtro, ordenacao, ordem } = req.query;

    //construir consulta SQL base
    let query = `SELECT * from cliente`;

    //adicionar clausura where onde tiver
    if (filtro) {
      //query = query + filtro - está incorreto
      //query = query + ` where ${filtro}`;
      query += ` where ${filtro}`;
    }

    //adicionar a clausula ordem by, quando houver
    if (ordenacao) {
      query += ` order by  ${ordenacao}`;

      //adicionar a ordem do order by (asc ou desc)
      if (ordem) {
        query += `  ${ordem}`;
      }// fim if ordem
    } // fim de ordenacao

    try {
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          res.status(500).json({ error: "Usuários não encontrados no banco" });
          return;
        } //fim do if

        console.log("Consulta realizada com sucesso!");
        res.status(201).json({ result });
        return;
      }); //fim do connect
    } catch (error) {
      console.error("Erro ao executar a consulta: ", error);
      res.status(500).json({ error: "Erro interno no servidor" });
      return;
    } //fim do catch
  } // fim do getAllClientes2
}; //fim do module
